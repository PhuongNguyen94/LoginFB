//
//  ViewController.swift
//  LoginApp
//
//  Created by Administrator on 1/19/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit

class ViewController: UIViewController {
    @IBOutlet weak var profileImage : UIImageView!
    @IBOutlet weak var LabelName : UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        let loginButton = FBSDKLoginButton()
        loginButton.center = view.center
        loginButton.readPermissions = ["public_profile", "email"]
        view.addSubview(loginButton)
        
        FBSDKProfile.loadCurrentProfile { (profile, error) in
            if (profile != nil){
                
                if let name = profile?.name{
                    self.LabelName.text = name
                }
                if let imageView = profile?.imageURL(for: FBSDKProfilePictureMode(rawValue: 1)!, size: CGSize(width: 300, height: 300)){
                   
                    let data = NSData.init(contentsOf: imageView)
                    self.profileImage.image = UIImage(data: data! as Data)
                    
                    print(imageView)
                }
                //MARK: - Check Login
                
                if FBSDKAccessToken.current() != nil{
                    
                    print("Dang nhap thanh cong")
              
                }
            }
           
            
            
                
        }
    
        
  
        
    }
    

    
}


