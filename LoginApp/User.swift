//
//  User.swift
//  LoginApp
//
//  Created by Administrator on 1/19/18.
//  Copyright © 2018 Administrator. All rights reserved.
//

import UIKit

class User {
    var name : String?
    var image : UIImageView?
    init(name:String,image:UIImageView){
        self.image = image
        self.name = name
    }
}
